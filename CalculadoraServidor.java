import OpAritmetica.*; // Importamos el paquete generado por idlj
import org.omg.CosNaming.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import java.util.Properties;

public class CalculadoraServidor 
{
    public static void main(String[ ] args) 
    {
        try 
        {
            // Creamos e inicializamos el ORB
            ORB orb = ORB.init(args, null);

            //Hacemos referencia al POA Raiz 
            POA rootPOA = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));

            //Activamos el POA manager
            rootPOA.the_POAManager().activate();

            //Crear instancia de la implementacion (servant)
            OperacionesImpl operacionesServant = new OperacionesImpl();

            //Obtener referencia de objeto del servidor
            org.omg.CORBA.Object operacionesCorba = rootPOA.servant_to_reference(operacionesServant);

            //Obtener el root naming context
            //NameService invokes the name service
            org.omg.CORBA.Object ncCorba = orb.resolve_initial_references("NameService");
            NamingContextExt nc = NamingContextExtHelper.narrow(ncCorba);

            //Asociamos un nombre
            nc.rebind(nc.to_name("Operaciones Aritmeticas"), operacionesCorba);
            
            //Quedar a la espera de peticiones
            System.out.println("Proceso servidor en espera ... ");
            orb.run();
        } catch (Exception e) {e.printStackTrace();}
        System.out.println("Calculadora Servidor Exiting ...");
    }
}