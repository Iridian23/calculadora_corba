import OpAritmetica.*; // Importamos el paquete generado por idlj
import org.omg.CosNaming.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.omg.CORBA.*;

public class CalculadoraCliente 
{
    public static void main(String[] args) 
    {
        try 
        {
            //Creamos e inicializamos el ORB
            ORB orb = ORB.init(args, null);

            //Obtener instancia del servidor de nombres (initial namingcontext)
            org.omg.CORBA.Object ncCorba=orb.resolve_initial_references("NameService");

            //Use NamingContextExt instead of NamingContext. This is 
            //part of the Interoperable naming Service. 
            NamingContextExt nc = NamingContextExtHelper.narrow(ncCorba);

            //Construir el nombre del objeto y obtener ref. desde serv. denombres
            org.omg.CORBA.Object operacionesCorba=nc.resolve(nc.to_name("Operaciones Aritmeticas"));

            //Convertir el objeto CORBA al tipo Calculadora (narrow)
            Operaciones operaciones = OperacionesHelper.narrow(operacionesCorba);

            //Invocamos metodos remotos
            char opc;
            do 
            {
            System.out.println("\nCALCULADORA CON CORBA");
            System.out.println("-----------------------------------------------");
            System.out.println(".- Elija la operacion a realizar");
            System.out.println("a) SUMA DE DOS NUMEROS");
            System.out.println("b) RESTA DE DOS NUMEROS");
            System.out.println("c) MULTIPLICACION DE DOS NUMEROS");
            System.out.println("d) DIVISION DE DOS NUMEROS");
            System.out.println("e) POTENCIA DE DOS NUMEROS");
            System.out.println("f) SALIR DEL PROGRAMA");
            System.out.println("-----------------------------------------------");
            BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
            String var = buffer.readLine();
            opc = var.charAt(0);

            if (opc == 'f') 
            {
                System.exit(0);
            }
            
            System.out.print("Ingrese el primer numero: ");
            var = buffer.readLine();
            double num1 = Double.parseDouble(var);
            System.out.println();

            System.out.print("Ingrese el segundo numero: ");
            var = buffer.readLine();
            double num2 = Double.parseDouble(var);
            System.out.println();
            switch (opc)
            {
            case 'a':
                System.out.println("La Suma de: "+num1+" mas "+num2+" es: "+ operaciones.Suma(num1,num2) + "\n\n");
                break;
            case 'b':
                System.out.println("La Resta de: "+num1+" menos "+num2+" es: "+ operaciones.Resta(num1,num2) + "\n\n");
                break;
            case 'c':
                System.out.println("La Multiplicacion de: "+num1+" * "+num2+" es: "+ operaciones.Multiplicacion(num1,num2) + "\n\n");
                break;
            case 'd':
                System.out.println("La Division de: "+num1+" / "+num2+" es: "+ operaciones.Division(num1,num2) + "\n\n");
                break;
            case 'e':
                System.out.println("La Potencia de: "+num1+" ^ "+num2+" es: "+ operaciones.Potencia(num1,num2) + "\n\n");
                break;
            case 'f':
                break;
            default:
            System.out.println("Error: Ingrese una operación correcta\n\n");
            break;
            }
            } while (opc != 'f');
        } catch (Exception e) {e.printStackTrace();}
    }
}