package OpAritmetica;


/**
* OpAritmetica/OperacionesPOA.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from calculadora.idl
* mi�rcoles 10 de junio de 2020 08:17:32 PM CDT
*/

public abstract class OperacionesPOA extends org.omg.PortableServer.Servant
 implements OpAritmetica.OperacionesOperations, org.omg.CORBA.portable.InvokeHandler
{

  // Constructors

  private static java.util.Hashtable _methods = new java.util.Hashtable ();
  static
  {
    _methods.put ("Suma", new java.lang.Integer (0));
    _methods.put ("Resta", new java.lang.Integer (1));
    _methods.put ("Multiplicacion", new java.lang.Integer (2));
    _methods.put ("Division", new java.lang.Integer (3));
    _methods.put ("Potencia", new java.lang.Integer (4));
  }

  public org.omg.CORBA.portable.OutputStream _invoke (String $method,
                                org.omg.CORBA.portable.InputStream in,
                                org.omg.CORBA.portable.ResponseHandler $rh)
  {
    org.omg.CORBA.portable.OutputStream out = null;
    java.lang.Integer __method = (java.lang.Integer)_methods.get ($method);
    if (__method == null)
      throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);

    switch (__method.intValue ())
    {
       case 0:  // OpAritmetica/Operaciones/Suma
       {
         double a = in.read_double ();
         double b = in.read_double ();
         double $result = (double)0;
         $result = this.Suma (a, b);
         out = $rh.createReply();
         out.write_double ($result);
         break;
       }

       case 1:  // OpAritmetica/Operaciones/Resta
       {
         double a = in.read_double ();
         double b = in.read_double ();
         double $result = (double)0;
         $result = this.Resta (a, b);
         out = $rh.createReply();
         out.write_double ($result);
         break;
       }

       case 2:  // OpAritmetica/Operaciones/Multiplicacion
       {
         double a = in.read_double ();
         double b = in.read_double ();
         double $result = (double)0;
         $result = this.Multiplicacion (a, b);
         out = $rh.createReply();
         out.write_double ($result);
         break;
       }

       case 3:  // OpAritmetica/Operaciones/Division
       {
         double a = in.read_double ();
         double b = in.read_double ();
         double $result = (double)0;
         $result = this.Division (a, b);
         out = $rh.createReply();
         out.write_double ($result);
         break;
       }

       case 4:  // OpAritmetica/Operaciones/Potencia
       {
         double a = in.read_double ();
         double b = in.read_double ();
         double $result = (double)0;
         $result = this.Potencia (a, b);
         out = $rh.createReply();
         out.write_double ($result);
         break;
       }

       default:
         throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);
    }

    return out;
  } // _invoke

  // Type-specific CORBA::Object operations
  private static String[] __ids = {
    "IDL:OpAritmetica/Operaciones:1.0"};

  public String[] _all_interfaces (org.omg.PortableServer.POA poa, byte[] objectId)
  {
    return (String[])__ids.clone ();
  }

  public Operaciones _this() 
  {
    return OperacionesHelper.narrow(
    super._this_object());
  }

  public Operaciones _this(org.omg.CORBA.ORB orb) 
  {
    return OperacionesHelper.narrow(
    super._this_object(orb));
  }


} // class OperacionesPOA
