import OpAritmetica.OperacionesPOA;
import java.math.*;
public class OperacionesImpl extends OperacionesPOA 
{
    public double Suma(double a, double b)
    {
        double suma;
        suma = a+b;
        return suma;
    }

    public double Resta(double a, double b)
    {
        double resta;
        resta = a-b;
        return resta;
    }

    public double Multiplicacion(double a, double b)
    {
        double multiplicacion;
        multiplicacion = a*b;
        return multiplicacion;
    }

    public double Division(double a, double b)
    {
        double division;
        division = a/b;
        return division;
    }

    public double Potencia(double a, double b)
    {
        double potencia;
        potencia = (double)Math.pow(a,b);
        return potencia;
    }
}